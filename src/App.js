import React, { Component } from 'react';

import './App.css';
import Key from './Key.js';
import Mask from './Mask.js';

const ALPHABET    = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
const DICTIONNARY = [
  'arthur',
  'karadoc',
  'perceval',
  'leodagan',
  'guenievre',
  'seli',
  'bohort',
  'merlin',
];

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      usedLetters: [],
      wordToGuess: this.pickRandomWord(),
      showReplay: false
    }
  }

  componentDidUpdate() {
    if (false === this.state.showReplay && !this.revealWord().split("").includes("_")) {
      this.setState({
        showReplay: true
      })
    }
  }

  pickRandomWord() {
    var word = DICTIONNARY[Math.floor(Math.random() * DICTIONNARY.length)];
    console.log(word)

    return word
  }

  getClassForKey(letter) {
    return this.state.usedLetters.includes(letter) ? "used" : ""
  }

  // For autobinding
  handleKeyClick = (letter) => {
    if(!this.state.usedLetters.includes(letter)) {
      this.setState({
        usedLetters: [ ...letter, ...this.state.usedLetters],
      })
    }
  }

  // For autobinding
  revealWord = () => {
    var revealed    = ""
    var wordAsArray = this.state.wordToGuess.split("").map((letter) => letter.toUpperCase())

    for(var i = 0, c = wordAsArray.length ; i < c ; i++) {
      if (this.state.usedLetters.includes(wordAsArray[i])) {
        revealed = revealed + wordAsArray[i]
      } else {
        revealed = revealed + "_"
      }
    }

    // split & join pour ajouter un espace, pour la lisibilité
    return revealed.split('').join(' ')
  }

  resetGame = () => {
    this.setState({
      usedLetters: [],
      wordToGuess: this.pickRandomWord(),
      showReplay: false
    })
  }

  render () {
    return (
      <div className="App">
        <h1>Devinez le mot !</h1>
        <Mask text={this.revealWord()} />
        {this.state.showReplay ? (
          <button
            className="replay"
            onClick={() => this.resetGame()}
          >
            Rejouer ?
          </button>) : ""}
        {ALPHABET.split("").map((letter) =>
          <Key
            letter={letter}
            key={letter}
            status={ this.getClassForKey(letter) }
            onClick={this.handleKeyClick}
          />
        )}
      </div>
    )
  }
}

export default App;
