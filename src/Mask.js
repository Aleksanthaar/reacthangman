import React from 'react';

import './Mask.css'

const Mask = ({ text }) => (
    <div className="mask">{text}</div>
)

export default Mask