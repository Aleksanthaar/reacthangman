import React from 'react';

import './Key.css'

const Key = ({ letter, status, onClick }) => (
    <div className={`key ${status}`} onClick={ () => onClick(letter) }>
        {letter}
    </div>
)

export default Key